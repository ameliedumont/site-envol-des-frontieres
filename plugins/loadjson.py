import datetime
import logging
import json
import os

from pelican import signals
from pelican.contents import Article
from pelican.readers import BaseReader

logger = logging.getLogger(__name__)

file_path = os.path.dirname(os.path.realpath(__file__))
filename = file_path + '/../themes/envol/static/js/data.json'
f = open(filename,)
data = json.load(f)

f.close()

def addArticle(articleGenerator):

    settings = articleGenerator.settings

    # Author, category, and tags are objects, not strings, so they need to
    # be handled using BaseReader's process_metadata() function.
    baseReader = BaseReader(settings)

    for i in reversed(data):
        content = data[i]['long_desc']

        article_data = {
            "title": i,
            "date": datetime.datetime.now(),
            "category": baseReader.process_metadata("category", "associations"),
            "summary": data[i]['short_desc'],
            "logo": data[i]['logo'],
            "filtres": data[i]['categories']
        }

        if data[i].get('site'):
            article_data['site'] = data[i]['site']

        if data[i].get('facebook'):
            article_data['facebook'] = data[i]['facebook']

        if data[i].get('twitter'):
            article_data['twitter'] = data[i]['twitter']

        if data[i].get('instagram'):
            article_data['instagram'] = data[i]['instagram']

        if data[i].get('mail'):
            article_data['mail'] = data[i]['mail']

        if data[i].get('contact'):
            article_data['contact'] = data[i]['contact']

        if data[i].get('telephone'):
            article_data['telephone'] = data[i]['telephone']

        if data[i].get('adresse'):
            article_data['adresse'] = data[i]['adresse']

        newArticle = Article(content, article_data)





        articleGenerator.articles.insert(0, newArticle)
        logger.info(i)



def register():
    signals.article_generator_pretaxonomy.connect(addArticle)
