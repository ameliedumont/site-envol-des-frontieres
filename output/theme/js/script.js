

let p_menu = document.getElementById("nav_menu");
let l_pages = document.getElementsByClassName("li_menu");
liste_pages = Array.from(l_pages);
let line = document.getElementById("line");

function menu_active() {
  for(let i=0; i<liste_pages.length; i++) {
    if(liste_pages[i].classList.contains("active")) {
      line.classList.add("menu_bas_fixe");
      for(let j=0; j<liste_pages.length; j++) {
        if(j < i) {
          liste_pages[j].classList.add("menu_bas_fixe");
        }
      }
    }
  }
}

window.onload = function() {
  menu_active();


  for(let i=0; i<liste_pages.length; i++) {
    liste_pages[i].addEventListener("mouseenter", function() {
      if(line.classList.contains("menu_bas_fixe")) {
        line.classList.remove("menu_bas_fixe");
      }
      if(liste_pages[i].classList.contains("menu_bas_fixe")) {
        liste_pages[i].classList.remove("menu_bas_fixe");
      }
      line.classList.add("menu_bas");
      for(let j=0; j<liste_pages.length; j++) {
        if(j < i) {
          if(liste_pages[j].classList.contains("menu_bas_fixe")) {
            liste_pages[j].classList.remove("menu_bas_fixe");
          }
          liste_pages[j].classList.add("menu_bas");
        }
      }
    });
    liste_pages[i].addEventListener("mouseleave", function() {
      for(let j=0; j<liste_pages.length; j++) {
        if(liste_pages[j].classList.contains("menu_bas")) {
          liste_pages[j].classList.remove("menu_bas");
          line.classList.remove("menu_bas");
        }
      }
      menu_active();
    });

  }
}




//liste_pages.forEach(element => console.log(element));
